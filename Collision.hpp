#pragma once

#include "Shape.hpp"

#include <SFML/System.hpp>

#include <vector>
#include <math.h>

class Collision {
	
	public:
	
		float min(float a, float b);
		float max(float a, float b);
		
		float dot_product(sf::Vector2f a, sf::Vector2f b);
		
		void project_to_axis(Shape* s, sf::Vector2f axis, float& min, float& max);
	
		bool update_minimum_overlap(int loop_index, float a_min, float b_max, int& min_axis_index, float& minimum_overlap);

		bool check_collision(Shape* a, Shape* b, bool resolve);
		
};