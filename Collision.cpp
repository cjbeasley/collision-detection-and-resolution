#include "Collision.hpp"

float Collision::min(float a, float b) {
	if (a == b) return a;
	if (a < b) return a;
	return b;
}

float Collision::max(float a, float b) {
	if (a == b) return a;
	if (a > b) return a;
	return b;
}

float Collision::dot_product(sf::Vector2f a, sf::Vector2f b) {
	return (a.x * b.x) + (a.y * b.y);
}

void Collision::project_to_axis(Shape* s, sf::Vector2f axis, float& min, float& max) {
	
	//Using the dot product to project each corner of the Shape to an axis
	//Explained more in Collision::check_collision
	std::vector<sf::Vector2f> points = s->get_points();
	float dot = dot_product(points[0], axis);
	min = dot;
	max = dot;
	
	//We only need the min and max values from the projection of points
	for (int i = 0; i < points.size(); ++i) {
		
		dot = dot_product(points[i], axis);
		
		if (dot < min) min = dot;
		else if (dot > max) max = dot;
		
	}
	
}

bool Collision::update_minimum_overlap(int loop_index, float a_min, float b_max, int& min_axis_index, float& minimum_overlap) {
	
	//Updating the minimum overlap of two shapes and from 
	//which separating axis the overlap occurred  
	
	float overlap = b_max - a_min;
	
	if (overlap < minimum_overlap) {
		minimum_overlap = overlap;
		min_axis_index = loop_index;
		return true;
	}
	
	return false;
	
}

bool Collision::check_collision(Shape* a, Shape* b, bool resolve) {
	
	std::vector<sf::Vector2f> a_axes = a->get_separating_axes();
	std::vector<sf::Vector2f> b_axes = b->get_separating_axes();
	
	float a_min, a_max;
	float b_min, b_max;
	
	float minimum_overlap = 999;
	
	int minimum_overlap_axis;
	
	bool is_from_a;
	
	sf::Vector2f mtv = sf::Vector2f(0, 0);
	
	//For each separating axis of Shape A, 
	//project corners of each shape to the axis
	//and then test for overlap
	for (int i = 0; i < a_axes.size(); ++i) {
				
		project_to_axis(a, a_axes[i], a_min, a_max);
		project_to_axis(b, a_axes[i], b_min, b_max);
		
		//If there is no overlap, there is no collision
		if (a_max < b_min || b_max < a_min)
			return false;
			
		//If we want to resolve the collision, update the minimum overlap
		//and mark where the minimum overlap came from
		if (resolve)
			if (update_minimum_overlap(i, a_min, b_max, minimum_overlap_axis, minimum_overlap))
				is_from_a = true;

	}
	
	for (int j = 0; j < b_axes.size(); ++j) {
		
		project_to_axis(a, b_axes[j], a_min, a_max);
		project_to_axis(b, b_axes[j], b_min, b_max);
		
		if (a_max < b_min || b_max < a_min)
			return false;
			
		if (resolve)
			if (update_minimum_overlap(j, a_min, b_max, minimum_overlap_axis, minimum_overlap))
				is_from_a = false;
				
	}
	
	if (resolve) {
		
		//Multiply minimum overlap by the separating axis
		//that returned it.
		//This provides the direction we need to resolve in.
		
		if (is_from_a) {
			mtv.x = minimum_overlap * a_axes[minimum_overlap_axis].x;
			mtv.y = minimum_overlap * a_axes[minimum_overlap_axis].y;
		} else {
			mtv.x = minimum_overlap * b_axes[minimum_overlap_axis].x;
			mtv.y = minimum_overlap * b_axes[minimum_overlap_axis].y;
		}
		
		a->move(mtv);
		
	}
	
	return true;
	
}