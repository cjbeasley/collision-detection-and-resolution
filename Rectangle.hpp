#pragma once

#include "Shape.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <math.h>

class Rectangle : public Shape {
	
	public:
	
		Rectangle(sf::Vector2f p, sf::Vector2f s);
		
		void move(sf::Vector2f offset) { rectangle.move(offset); }
		void set_movable(bool m) { movable = m; }
		
		void movement();
		void update();
		void draw(sf::RenderWindow* window);
		
	private:
	
		bool movable = false;
		sf::Clock clock;
	
		sf::RectangleShape rectangle;
		
		sf::Vector2f position, size;
		
		float rotation_degrees = 0, rotation_radians = 0;
		
		sf::Vector2f top_left();
		sf::Vector2f top_right();
		sf::Vector2f bottom_left();
		sf::Vector2f bottom_right();
		
};