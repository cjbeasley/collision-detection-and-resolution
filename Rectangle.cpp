#include "Rectangle.hpp"

Rectangle::Rectangle(sf::Vector2f p, sf::Vector2f s) {
	
	size = s;
	rectangle.setPosition(p);
	rectangle.setSize(s);
	
	//Graphical Representation
	rectangle.setOutlineThickness(1);
	rectangle.setOutlineColor(sf::Color::Cyan);
	rectangle.setFillColor(sf::Color::Black);
	
	//Set origin of rectangle to center
	rectangle.setOrigin(s.x/2, s.y/2); 
	
	points = std::vector<sf::Vector2f>(4);
	separating_axes = std::vector<sf::Vector2f>(4);
	
}

void Rectangle::update() {
	
	position = rectangle.getPosition();
	rotation_degrees = rectangle.getRotation();
	rotation_radians = rotation_degrees * PI/180;
	
	points[0] = top_left();
	points[1] = top_right();
	points[2] = bottom_right();
	points[3] = bottom_left();
	
	//Calculate normals for each edge
	float e1x = points[0].x - points[1].x;
	float e1y = points[0].y - points[1].y;
	separating_axes[0] = normalize(sf::Vector2f(-e1y, e1x));
	
	float e2x = points[1].x - points[2].x;
	float e2y = points[1].y - points[2].y;
	separating_axes[1] = normalize(sf::Vector2f(-e2y, e2x));
	
	float e3x = points[2].x - points[3].x;
	float e3y = points[2].y - points[3].y;
	separating_axes[2] = normalize(sf::Vector2f(-e3y, e3x));
	
	float e4x = points[3].x - points[0].x;
	float e4y = points[3].y - points[0].y;
	separating_axes[3] = normalize(sf::Vector2f(-e4y, e4x));
	
	movement();
	
}

void Rectangle::draw(sf::RenderWindow* window) {
	
	window->draw(rectangle);
	
}

void Rectangle::movement() {
	
	if (movable) {
		
		sf::Time elapsed = clock.restart();
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			rectangle.setRotation(rotation_degrees+0.05);
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			rectangle.move(-250 * elapsed.asSeconds(), 0);
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			rectangle.move(250 * elapsed.asSeconds(), 0);
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			rectangle.move(0, -250 * elapsed.asSeconds());
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			rectangle.move(0, 250 * elapsed.asSeconds());
		
	}
	
}

sf::Vector2f Rectangle::top_left() {
	
	//find the unrotated top left corner
	float x = position.x - size.x / 2;
	float y = position.y - size.y / 2;
	
	//translate top left corner to where it would be
	//if the rectangle was centered at (0, 0)
	float translated_x = x - position.x;
	float translated_y = y - position.y;
	
	//calculate the position of the rotated top left corner
	float rotated_x = translated_x * cos(rotation_radians) - translated_y * sin(rotation_radians);
	float rotated_y = translated_x * sin(rotation_radians) + translated_y * cos(rotation_radians);
	
	//translate the top left corner back to its original position
	float result_x = rotated_x + position.x;
	float result_y = rotated_y + position.y;
	
	return sf::Vector2f(result_x, result_y);
	
}

sf::Vector2f Rectangle::top_right() {
	
	//find the unrotated top right corner
	float x = position.x + size.x / 2;
	float y = position.y - size.y / 2;
	
	//translate top left corner to where it would be
	//if the rectangle was centered at (0, 0)
	float translated_x = x - position.x;
	float translated_y = y - position.y;
	
	//calculate the position of the rotated top left corner
	float rotated_x = translated_x * cos(rotation_radians) - translated_y * sin(rotation_radians);
	float rotated_y = translated_x * sin(rotation_radians) + translated_y * cos(rotation_radians);
	
	//translate the top left corner back to its original position
	float result_x = rotated_x + position.x;
	float result_y = rotated_y + position.y;
	
	return sf::Vector2f(result_x, result_y);
	
}

sf::Vector2f Rectangle::bottom_right() {
	
	//find the unrotated top right corner
	float x = position.x + size.x / 2;
	float y = position.y + size.y / 2;
	
	//translate top left corner to where it would be
	//if the rectangle was centered at (0, 0)
	float translated_x = x - position.x;
	float translated_y = y - position.y;
	
	//calculate the position of the rotated top left corner
	float rotated_x = translated_x * cos(rotation_radians) - translated_y * sin(rotation_radians);
	float rotated_y = translated_x * sin(rotation_radians) + translated_y * cos(rotation_radians);
	
	//translate the top left corner back to its original position
	float result_x = rotated_x + position.x;
	float result_y = rotated_y + position.y;
	
	return sf::Vector2f(result_x, result_y);
	
}

sf::Vector2f Rectangle::bottom_left() {
	
	//find the unrotated top right corner
	float x = position.x - size.x / 2;
	float y = position.y + size.y / 2;
	
	//translate top left corner to where it would be
	//if the rectangle was centered at (0, 0)
	float translated_x = x - position.x;
	float translated_y = y - position.y;
	
	//calculate the position of the rotated top left corner
	float rotated_x = translated_x * cos(rotation_radians) - translated_y * sin(rotation_radians);
	float rotated_y = translated_x * sin(rotation_radians) + translated_y * cos(rotation_radians);
	
	//translate the top left corner back to its original position
	float result_x = rotated_x + position.x;
	float result_y = rotated_y + position.y;
	
	return sf::Vector2f(result_x, result_y);
	
}
