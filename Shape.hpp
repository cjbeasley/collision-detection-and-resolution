#pragma once

#include <SFML/System.hpp>

#include <vector>
#include <math.h>

class Shape {
	
	public:
	
		std::vector<sf::Vector2f> get_points() { return points; }
		std::vector<sf::Vector2f> get_separating_axes() { return separating_axes; }
	
		virtual void move(sf::Vector2f offset) = 0;
	
	
		//We need to normalize each separating axis
		//for our collision code to find values needed for resolution
		sf::Vector2f normalize(sf::Vector2f v) {
			
			sf::Vector2f tv = v;
			float length = sqrt( (tv.x * tv.x) + (tv.y * tv.y) );
			
			if (length != 0) {
				tv.x = tv.x/length;
				tv.y = tv.y/length;
			}
			
			return tv;
			
		}
		
	protected:
	
		//The separating axes are the normals of each edge of the shape
		//In other words, for each edge we need a vector perpendicular to that edge
		std::vector<sf::Vector2f> separating_axes;

		std::vector<sf::Vector2f> points;
		
		float PI = 3.1415926;

};