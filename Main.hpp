#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include "Rectangle.hpp"
#include "Collision.hpp"

#include <iostream>

int main() {
	
	bool running = true;
	
	sf::RenderWindow window;
	sf::Event event;
	
	window.create(sf::VideoMode(1366, 768), "Collision", sf::Style::Close | sf::Style::Titlebar);

	sf::Font font;
	
	if (!font.loadFromFile("munro.ttf"))
		std::cout << "Could not load font file" << std::endl;
	
	sf::Text instructions;
	instructions.setFont(font);
	instructions.setString("Move with Arrow Keys  ||  Rotate with R");
	instructions.setCharacterSize(30);
	instructions.setOutlineColor(sf::Color::Cyan);
	instructions.setFillColor(sf::Color::Cyan);
	instructions.setPosition(25, 25);
	
	Collision collider;
	
	Rectangle movable(sf::Vector2f(300, 300), sf::Vector2f(30, 30));
	Rectangle one(sf::Vector2f(200, 200), sf::Vector2f(50, 50));
	Rectangle two(sf::Vector2f(400, 400), sf::Vector2f(50, 50));
	Rectangle three(sf::Vector2f(1000, 300), sf::Vector2f(50, 50));
	Rectangle four(sf::Vector2f(900, 500), sf::Vector2f(50, 50));
	Rectangle five(sf::Vector2f(700, 400), sf::Vector2f(50, 50));
	
	movable.set_movable(true);
	
	one.update();
	two.update();
	three.update();
	four.update();

	
	while (running) {
		
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				running = false;
		}
		
		movable.update();
		five.update();
		
		collider.check_collision(&movable, &one, true);
		collider.check_collision(&movable, &two, true);
		collider.check_collision(&movable, &three, true);
		collider.check_collision(&movable, &four, true);
		
		collider.check_collision(&five, &one, true);
		collider.check_collision(&five, &two, true);
		collider.check_collision(&five, &three, true);
		collider.check_collision(&five, &four, true);
		
		collider.check_collision(&movable, &five, true);
		collider.check_collision(&five, &movable, true);
		
		window.clear(sf::Color::Black);
		
		movable.draw(&window);
		one.draw(&window);
		two.draw(&window);
		three.draw(&window);
		four.draw(&window);
		five.draw(&window);
		
		window.draw(instructions);
		
		window.display();
		
	}
	
	window.close();
	
	return 0;
	
}